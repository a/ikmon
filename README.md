# ikmon

ikmon is a quick, tiny script that can be used to figure out how much you spent in the last month (or any other time frame if you change one line in the source code) with your istanbulkart, assuming that it's added to your account.

You need to set a mobile pin by using the android or ios app first to make this work.

It started as a result of me and a coworker talking about deciding if we want to go for a monthly subscription (180 rides for a month, costs 275TRY in 2020), and I quickly hacked this together while we were on marmaray.

## Screenshots

![Output of ikmon](https://elixi.re/i/e1ciqf2d.png)

So no, it's not worth getting a subscription for me at this time, especially considering that I often am forced to use other forms of transportation that don't support istanbulkart.

## Usage

- Install python3.6+
- Install requirements in requirements.txt (`pip3 install -U -r requirements.txt`)
- Run ikmon `python3 ikmon.py`

import requests
from decimal import Decimal
from datetime import datetime, timedelta


def do_auth(email, pin):
    """Log into an istanbulkart account using pin"""
    pinlogin = requests.post(
        "https://mobiletugra.belbim.istanbul/MobileAdapter/api/MobileAdapter/MBLoginWithPin",
        json={
            "ApplicationVersion": "2.0.17",
            "DeviceId": "",
            "Email": email,
            "FCMRegistrationId": "",
            "GUIDNumber": "",
            "Language": "tr-TR",
            "MobilePin": pin,
            "OSTypeId": 1,
        },
        headers={"User-Agent": "okhttp/2.6.0"},
    )
    return pinlogin.json()


def get_card_list(cnum, token):
    """Get istanbulkart list of the user"""
    cardlist = requests.post(
        "https://mobiletugra.belbim.istanbul/MobileAdapter/api/MobileAdapter/MBCardList",
        json={"CustomerNumber": cnum, "Language": "tr-TR", "SessionToken": token},
        headers={"User-Agent": "okhttp/2.6.0"},
    )
    return cardlist.json()


def get_transactions(cnum, token, cardid, startdate, enddate):
    """Get list of transactions of the user"""
    txlist = requests.post(
        "https://mobiletugra.belbim.istanbul/MobileAdapter/api/MobileAdapter/MBCardTransactionList",
        json={
            "CardId": cardid,
            "CustomerNumber": cnum,
            "EndDate": enddate,
            "Language": "tr-TR",
            "SessionToken": token,
            "StartDate": startdate,
        },
        headers={"User-Agent": "okhttp/2.6.0"},
    )
    return txlist.json()


if __name__ == "__main__":
    print("ikmon | https://gitlab.com/a/ikmon | AGPLv3")

    measuredays = 31
    email = input("Email: ")
    pin = input("Mobile pin: ")

    pinloginj = do_auth(email, pin)

    # Get the customer number and token
    cnum = pinloginj["MBResponseMessage"]["CustomerInfo"]["CustomerNumber"]
    token = pinloginj["MBResponseMessage"]["SessionToken"]

    cardlistj = get_card_list(cnum, token)

    # Calculate timestamps
    startdate = (datetime.now() - timedelta(days=measuredays)).strftime(
        "%d.%m.%Y %H:%M:%S"
    )
    enddate = datetime.now().strftime("%d.%m.%Y %H:%M:%S")

    # Loop through all cards
    for card in cardlistj["MBResponseMessage"]["CardList"]:
        txlj = get_transactions(cnum, token, card["CardId"], startdate, enddate)
        moneyin = 0
        refunds = 0
        moneyout = 0

        # Loop through transactions
        for tx in txlj["MBResponseMessage"]["TransactionList"]:
            amount = Decimal(tx["TrnAmount"].replace(",", "."))
            # HACK: Can we clean this up, like, at all?
            # Decide whether if amount is inwards or outwards
            if "Talimat Yükleme" in tx["TrnCodeDescription"] or "Son Dolum" in tx["TrnCodeDescription"]:
                continue
            elif "İade" in tx["TrnCodeDescription"]:
                refunds += amount
            elif (
                "Yükleme" in tx["TrnCodeDescription"]
                or "Dolum" in tx["TrnCodeDescription"]
            ):
                moneyin += amount
            else:
                moneyout += amount

        print(
            f"{card['Name']} ({card['CardId']} - {card['MifareId']}): -{moneyout - refunds}TRY +{moneyin}TRY (last {measuredays} days)"
        )
